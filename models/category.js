var multilang 	= require('../config/multilang'),
	mongoose 	= require('mongoose');


// Definimos el esquema de Categoría
var Category = mongoose.model('Category', {
	identifier: { type:String, unique:true, trim:true, lowercase:true },
	name: 		multilang.multilang_mongo_docs,
	activated: 	Boolean,
	created: 	{ type:Date, default:Date.now },
});



/**
 *	[findAll: buscar todos los registros de categoría]
 */
exports.findAll = function(callback) {

	Category.find( function(err, categories) {
		if (err) {
			callback(err);
		} else {
			callback(null, categories);
		}
	});

};


/**
 *	[createCategory: Creación de categoría]
 */
exports.createCategory = function(post, callback) {

	// Definimos el modelo Categoria
	var category = new Category({
		identifier: post.identifier,
		activated: post.activated,
		name: post.name
	});


	// Y guardamos. Si hay error devolvemos err, sino, nada. 
	category.save(function(err) {
		if (err) {
			callback(err);
		} else {
			callback(null);
		}
	});

};


/**
 *	[editCategory: Editar categoría]
 */
exports.editCategory = function(post, callback)
{
	Category.findOneAndUpdate({'identifier' : post.identifier}, {

		activated: post.activated,
		name: post.name

	}, {}, function(err, category) {

		if (err) {
			callback(err);
		} else {
			callback(null);
		}

	});
}




/**
 *	[deleteCategory: Elimina categoría]
 */
exports.deleteCategory = function(identifier, callback)
{
	Category.findOneAndRemove({'identifier' : identifier}, {}, function(err) {
		if (err) {
			callback(err);
		} else {
			callback(null);
		}
	});
}


exports.findByIdentifier = function(identifier, callback)
{
	Category.findOne({'identifier' : identifier}, {}, function(err, category) {
		if (err) {
			callback(err);
		} else {
			callback(null, category);
		}
	});
}