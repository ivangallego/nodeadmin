var Category 	= require('../models/category')
	multilang	= require('../config/multilang');

/**
 *	[Index: Listado de categorías]
 */
exports.index = function(request, response)
{

	Category.findAll(function(err, categories) {		
		if (err) {

			// Si hay algún error con la query mostramos el error con un estado HTTP 500
			response.send(err, 500);

		} else {

			// Si todo ok, mostramos la vista "categories/index" con el correspondiente listado.
			response.render('categories/index', {
				title: 'nodeadmin CMS | Categorías',
				username: 'Iván Gallego',
				result: categories 
			});

		}
	});

};


/**
 *	[createForm: Formulario de creación de categorías]
 */
exports.createForm = function(request, response)
{
	response.render('categories/create', {
		title: 'nodeadmin CMS | Nueva Categoría',
		username: 'Iván Gallego',
		languages: multilang.langs,
		errors: [{}],
		post: [{}]
	});
};


/**
 *	[createPost: Validación y creación de categoría]
 */
exports.createPost = function(request, response)
{
	var post = request.body;
	post.identifier = Math.random().toString(36).substring(7);

	// Reglas de validación. Nombre al ser multi-idioma lo ponemos en un for siendo la key el código del idioma (To-do) 
	request.assert('activated', 'Selecciona si la categoría estará activada o no, por favor.').notEmpty();

	var errors = request.validationErrors();

	// Comprobamos si han habido errores...
	if ( errors) {

		// Hay errores. Volvemos al formulario con lo escrito anteriormente y con los mensajes de validación
		Category.findAll(function(err, categories) {
			response.render('categories/create', {
				title: 'nodeadmin CMS | Nueva Categoría',
				username: 'Iván Gallego',
				languages: multilang.langs,
				errors: errors,
				post: post
			});
		});

	} else {

		// Validación correcta, intentamos crear la categoria. Si falla, volvemos a redirigir al formulario.
		Category.createCategory(post, function(err) {
			if (err) {
				response.render('categories/create', {
					title: 'nodeadmin CMS | Nueva Categoría',
					username: 'Iván Gallego',
					languages: multilang.langs,
					errors: errors,
					post: post
				});		

			} else {
				response.redirect('/categories');
			}
		});

	}
}


/**
 *	[editForm: Formulario de edición de categorías]
 */
exports.editForm = function(request, response)
{
	var post = request.params;

	request.assert('identifier', 'El identificador no se ha pasado').notEmpty();

	var errors = request.validationErrors();

	if (errors) {
		response.redirect('/categories');
	} else {

		Category.findByIdentifier(post.identifier, function(err, category) {

			if (err) {
				response.redirect('/categories');
			} else {
				response.render('categories/edit', {
					title: 'nodeadmin CMS | Nueva Categoría',
					username: 'Iván Gallego',
					languages: multilang.langs,
					errors: [{}],
					post: category
				});
			}

		});

	}

}


/**
 *	[editPost: Validación y edición de categoría]
 */
exports.editPost = function(request, response)
{
	var post = request.body;

	// Reglas de validación. Nombre al ser multi-idioma lo ponemos en un for siendo la key el código del idioma (To-do) 
	request.assert('activated', 'Selecciona si la categoría estará activada o no, por favor.').notEmpty();

	var errors = request.validationErrors();

	// Comprobamos si han habido errores...
	if ( errors) {

		// Hay errores. Volvemos al formulario con lo escrito anteriormente y con los mensajes de validación
		Category.findAll(function(err, categories) {
			response.render('categories/create', {
				title: 'nodeadmin CMS | Nueva Categoría',
				username: 'Iván Gallego',
				languages: multilang.langs,
				errors: errors,
				post: post
			});
		});

	} else {

		Category.editCategory(post, function(err){

			if (!err) {
				response.redirect('/categories');
			} else {
				response.render('categories/create', {
					title: 'nodeadmin CMS | Nueva Categoría',
					username: 'Iván Gallego',
					languages: multilang.langs,
					errors: errors,
					post: post
				});
			}

		});

	}
}




/**
 *	[delete: Elimina categoría]
 */
exports.delete = function(request, response) 
{
	var post = request.params;

	request.assert('identifier', 'El identificador no se ha pasado').notEmpty();

	var errors = request.validationErrors();

	if (errors) {
		response.redirect('/categories');
	} else {

		Category.deleteCategory(post.identifier, function(err) {
			console.log(err);
			response.redirect('/categories');
		});

	}
}