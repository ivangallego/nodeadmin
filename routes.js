app = module.parent.exports.app;


// Require controllers
var staticController      	= require('./controllers/staticController'),
	categoriesController	= require('./controllers/categoriesController');


// Rutas estáticas
app.get('/', staticController.index);


// Rutas relativas a categorías
app.get('/categories', categoriesController.index);
app.get('/categories/create', csrf,	categoriesController.createForm);
app.post('/categories/create', csrf, categoriesController.createPost);
app.get('/categories/:identifier/edit', csrf, categoriesController.editForm);
app.post('/categories/:identifier/edit', csrf, categoriesController.editPost);
app.get('/categories/:identifier/delete', csrf, categoriesController.delete);





function csrf(req, res, next) {
    res.locals.token = req.session._csrf;
    next();
}