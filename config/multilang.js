
// Lang por defecto
exports.default_lang = 'es';


// Langs disponibles
exports.langs = {
	'es': { 'name':'Castellano', 	'code':'es'  },
	'en': { 'name':'English',		'code':'en'  },
};


// MongoDB multilang fields
exports.multilang_mongo_docs = { es:String, en:String };