
/**
 * [setDevelopmentConfig: Configuración entorno desarrollo]
 */
function setDevelopmentConfig(){
    DatabaseConfig.port = 61797;
    DatabaseConfig.host = 'ds061797.mongolab.com';
    DatabaseConfig.name = 'mionce';
    DatabaseConfig.user = 'labmioncecloud';
    DatabaseConfig.pass = '52OncemyclouD31';

    EnvConfig.port = 3000;
};


/**
 * [setDevelopmentConfig: Configuración entorno producción]
 * TODO: rellenar correctamente con los datos definitivos
 */
function setProductionConfig(){

    /* TODO */
    DatabaseConfig.port = 29017;
    DatabaseConfig.host = '';
    DatabaseConfig.name = '';
    DatabaseConfig.user = '';
    DatabaseConfig.pass = '';

    EnvConfig.port = 80;
};



var DatabaseConfig = { port:Number, host:String, name:String, user:String, pass:String };
var EnvConfig = { port:Number };

module.exports.DatabaseConfig       = DatabaseConfig;
module.exports.EnvConfig            = EnvConfig;
module.exports.setDevelopmentConfig = setDevelopmentConfig;
module.exports.setProductionConfig  = setProductionConfig;