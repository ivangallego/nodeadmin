var express             = require('express'),
    expressValidator    = require('express-validator'),
    config              = require('./config/config'),
    app                 = express();


// Avisamos a node en qué entorno estamos (meramente informativo)
process.env.NODE_ENV = (process.env.NODE_ENV || "development");

// Configuración de la App
app.configure(function() {
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.locals.pretty = true;
    app.use(express.bodyParser({uploadDir:'./uploads'}));
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    app.use(express.session({ secret:'fa2gGQH&5hl1315FAS' }));
    app.use(express.static(__dirname + '/public/'));
    app.use(expressValidator);
    app.use(express.csrf());
});


// Configuración específica del entorno de desarrollo. (Sustituir con entorno de producción al subir app)
app.configure('development', function(){
    config.setDevelopmentConfig();
    console.log(config.DatabaseConfig);
    console.log(config.EnvConfig);

    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});


// Conexión a MongoBD mediante www.mongolabs.com (mongoDB en la nuve)
var mongoose = require('mongoose');
mongoose.connect('mongodb://' + config.DatabaseConfig.user + ':' + config.DatabaseConfig.pass + '@' + config.DatabaseConfig.host + ':' + config.DatabaseConfig.port + '/' + config.DatabaseConfig.name);


// Preparamos la app para que escuche por el puerto indicado en configuración
app.listen(config.EnvConfig.port, function() {
    console.log('Express server listening on port %d in %s mode', config.EnvConfig.port, app.settings.env);
});


// Exportamos app para hacerla accesible
module.exports.app  = app;
routes              = require('./routes');